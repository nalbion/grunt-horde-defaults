// Load all of the Grunt plugins required
// Adapted from load-grunt-tasks
// Return a hash of module names, each set to true, for grunt-horde
'use strict';

var fs = require('fs');
var path = require('path');
var findup = require('findup-sync');
var multimatch = require('multimatch');

/**
 * @param originalTasks
 * @param patterns
 * @param config
 * @returns {Object.<string, boolean>} true for each task of the NPM module dependencies
 */
function findNpmTasks(originalTasks, patterns, config) {
	var scope = ['dependencies', 'devDependencies', 'peerDependencies'];
	if (typeof config === 'string') {
		config = require(config);
	}

	// @type {Object} prevTask - added to for each 'prop', returned as 'tasks'
	// @type {Object} prop - one of the dev/dependencies sections from the package.json file
	var tasks = scope.reduce(function (prevTasks, prop) {
		if( undefined !== config[prop] ) {
			// Find all dependencies that match the given patterns
			multimatch( Object.keys(config[prop]), patterns ).forEach( function(module) {
				var tasksPath = path.resolve(__dirname, '..', module, 'tasks');
				if( fs.existsSync(tasksPath) ) {
					prevTasks[module] = true;
//					fs.readdirSync( tasksPath ).forEach( function(taskFile) {
//						var match = taskFile.match(/^(.*)\.js$/);
//						if( match !== null ) {
//							prevTasks[match[1]] = true;
//						}
//					});
				}
			});
		}
		return prevTasks;
	}, {});

	for (var attrname in tasks) { originalTasks[attrname] = tasks[attrname]; }
	return originalTasks;
}

module.exports = function(grunt) {
	// Find all grunt- dependencies specified in the app's package.json,
	// but skip grunt-cli, grunt-horde and this module
	var appModulePatterns = ['grunt-*', '!grunt', '!grunt-cli', '!grunt-horde',
		'!' + path.basename( path.resolve(__dirname) )];
	var libModulePatterns = ['grunt-*', '!grunt-horde'];

	var tasks = findNpmTasks( {}, appModulePatterns, findup('package.json') );
	return tasks; // findNpmTasks( tasks, libModulePatterns, path.resolve(__dirname, 'package.json') );
};

