// Defines the arguments passed to grunt.registerTask
module.exports = function(grunt) {
	'use strict';

	var horde = this;

	/**
	 * Tasks can be configured for each app:
	 * <pre>
	 *     horde.demand('registerTask.configuredTasks.serve.dev', ['task1', 'task2']
	 * </pre>
	 *
	 * @param {string} task - "serve" or "closure"
	 * @param {string} target - "dev", "dist" etc
	 * @param {Object.<string, Array.<string>>} defaults (optional)
	 *
	 * @return {Array.<string>}
	 */
	var getTasksForTarget = function(task, target, defaults) {
		var key = 'registerTask.configuredTasks.' + task;
		var tasks = horde.learn(key);
		if( undefined !== tasks && tasks.construct == Array ) {
			grunt.log.write('Using custom tasks for ' + key + ': ' + tasks);
			return tasks;
		}

		if( null !== target ) {
			// Try the target-specific key
			tasks = horde.learn(key + '.' + target);
			if( undefined !== tasks ) {
				grunt.log.write('Using custom tasks for ' + key + '.' + target + ': ' + tasks);
			}
		}
		if( undefined === tasks && undefined !== defaults ) {
			if( null == target ) {
				tasks = defaults;
				grunt.log.debug('Tasks for "' + task + '": ' + tasks);
			} else {
				tasks = defaults[target];
				grunt.log.debug('Tasks for "' + task + ':' + target + '": ' + tasks);
			}
		}
		return tasks;
	}

	/**
	 * @param {string} task - "serve" or "closure"
	 * @param {string} target - "dev", "dist" etc
	 * @param {Object.<string, Array.<string>>} defaults (optional)
	 */
	var runTasksForTarget = function( task, target, defaults ) {
		var tasks = getTasksForTarget( task, target, defaults );

		if( tasks !== undefined ) {
			return grunt.task.run(tasks);
		} else {
			grunt.fatal('No tasks defined for ' + task + ':' + target);
		}
	}

	var getTaskAliasList = function( task, defaults ) {
		var a = [];
		a.push( getTasksForTarget(task, null, defaults) );
		return a;
	}

	return {
		default: getTaskAliasList('default', ['mavenPrepare:dist', 'dist', 'mavenDist']),
		qa: getTaskAliasList('qa', []), //'newer:jshint:src', test', 'sonarRunner']),
		build: getTaskAliasList('build', ['newer:less:dev', 'newer:copy']),
		dist: getTaskAliasList('dist', ['clean:dist', 'qa', 'less:dist', 'copy:dist', 'img']),

		serve: ['Run a Connect server with Watch (:dev, :proxy or :test)',
			function(target){
				runTasksForTarget( 'serve', (target || 'dev'), {
					test: [ // 'clean:frontend',
						'connect:test', 'open:test', 'watch:test'
					],
					proxy: [ //	'clean:server',
						'connect:frontend', 'configureProxies:proxy', 'connect:proxy',
						'open:dev', 'watch:livereload'
					],
					dev: [ 'clean:server', 'connect:frontend', 'open:dev', 'watch' ]
				} );
			}
		],

		closure: ['Compile javascript using Closure Compiler (:dev or :dist)',
			function(target) {
				runTasksForTarget( 'closure', (target || 'dev'), {
					dist: [//'mavenPrepare',
							'closureSoys',
							'closureDepsWriter:app',
							'closureBuilder:dist'],
					dev: ['closureSoys',
							'closureDepsWriter:app',
							'closureBuilder:dev',
							'replace:closure_source_map_paths']
				} );
			}
		]
	};
};
