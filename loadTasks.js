// Each returned key should refer to a task file.
// false can be used to disable a task path enabled in a module earlier in the merge.

// I thought the the following would help grunt-horde resolve transitive dependencies, but it didn't.
// ...I'm still not sure how this is supposed to be used...

//var fs = require('fs');
//var path = require('path');
//var findup = require('findup-sync');
//var multimatch = require('multimatch');
//
//function findTasks(originalTasks, patterns, config) {
//	var scope = ['dependencies', 'devDependencies', 'peerDependencies'];
//	if (typeof config === 'string') {
//		config = require(config);
//	}
//
//	// @type {Object} prevTask - added to for each 'prop', returned as 'tasks'
//	// @type {Object} prop - one of the dev/dependencies sections from the package.json file
//	var tasks = scope.reduce(function (prevTasks, prop) {
//		if( undefined !== config[prop] ) {
//			// Find all dependencies that match the given patterns
//			multimatch( Object.keys(config[prop]), patterns ).forEach( function(module) {
//				var tasksPath = path.resolve(__dirname, '..', module, 'tasks');
//				if( fs.existsSync(tasksPath) ) {
//					fs.readdirSync( tasksPath ).forEach( function(taskFile) {
//						var match = taskFile.match(/^(.*)\.js$/);
//						if( match !== null ) {
////              prevTasks[match[1]] = true;
//							prevTasks[path.relative( path.resolve(__dirname, '..', '..'),
//								path.resolve(tasksPath, taskFile) )] = true;
//						}
//					});
//				}
//			});
//		}
//		return prevTasks;
//	}, {});
//
//	for (var attrname in tasks) { originalTasks[attrname] = tasks[attrname]; }
//	return originalTasks;
//}

module.exports = function(grunt) {
	'use strict';

	// Find all grunt- dependencies specified in the app's package.json,
	// but skip grunt-cli, grunt-horde and this module
//  var appModulePatterns = ['grunt-*', '!grunt', '!grunt-cli', '!grunt-horde',
//      						'!' + path.basename( path.resolve(__dirname) )];
//  var libModulePatterns = ['grunt-*', '!grunt-horde'];
//
//  var tasks = findTasks( {}, appModulePatterns, findup('package.json') );
//  return findTasks( tasks, libModulePatterns, path.resolve(__dirname, 'package.json') );

	return {};
};