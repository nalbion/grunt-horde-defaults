# Common config for Grunt tasks
See https://github.com/codeactual/grunt-horde


## Usage
The defaults and tasks provided here may or may not suit your needs.  If they don't,
you can either over-ride them locally, or fork this project.

Your `package.json` should be fairly simple:

    {
      "name": "my-project",
      "version": "0.0.1",
      "description": "bla bla bla",
      "devDependencies": {
        "grunt": "~0.4.2",
        "grunt-horde": "~0.3.5",
        "grunt-horde-defaults": "git+ssh://git@<GIT_SERVER>:<GIT_USER_ID>/grunt-horde-defaults.git",
        "application-specific-dependency": "~1.0.0"
      }
    }

Your `Gruntfile.js` should also be fairly simple:

    module.exports = function(grunt) {
    	require('grunt-horde').create(grunt)
    	    .loot('grunt-horde-defaults')
    //		.loot('./config/grunt')
    //		.demand('initConfig.jshint.options', {node: true})
    		.attack();
    };

The `Gruntfile.js` for grunt-horde-defaults is *extremely* simple:

    module.exports = function(grunt) {
    };

All of the magic is done by `grunt-horde` and `grunt-horde-default`'s `loadNpmTasks`,
which you should never have to modify.  All you need to do is:

 - Add the dependencies to the `package.json` file of either your app or your fork of `grunt-horde-defaults`.
 - Add/edit the scripts in the `initConfig` directory of either your app or your fork of `grunt-horde-defaults`.

## Module Configuration

### Closure
If using Google's [Closure Compiler](https://developers.google.com/closure/compiler/), you need to provide:
 - `CC_HOME` environment variable which points to the directory containing `closure-library` - eg `C:\tools\closure`.
    (Alternatively, add `closure-library` to the directory containing your Gruntfile)
 - a `grunt-closure.json` file in the same directory as your Gruntfile

If you want to process JavaScript and/or Soy files that belong to bower modules, you need to add a `componentPath`
and a `component` property to your `grunt-closure.json` file:

    {
      "jsSrcPath": "app/js-closure/",
      "destPath": "dist/js/",
      "closureLibrary": "bower_components/closure-library/",
      "bootstrapFile": "relative-to-jsSrcPath.js",
      "sourceMap": "dist/js/my-app.js.map",
      "outputWrapper": "(function(){%output%}).call(this); //# sourceMappingURL=/js/my-app.js.map",

      "componentPath": "bower_components/",
	  "components": ["some-component", "another-component"]
	}

#### Soy Templates
If you have [Soy templates](https://developers.google.com/closure/templates/) to compile, add a `soySrcPath` property to your `grunt-closure.json` file:

    {
       "soySrcPath": "app/soy/"
    }

#### Internationalisation
To generate an XTB file from calls to `goog.getMsg()` run:

	grunt xtbGenerator

This will generate `app/messages.xtb` which you should have translated

### Connect
To add extra paths to the middleware path list use something like:

	// directory name
    horde.demand("initConfig.connect.extraMiddleware", "my/web/app");
    // and/or middleware functions
    horde.demand("initConfig.connect.extraMiddleware", [ query(), myMiddleware() ]);

### LESS
By default, `grunt less` will generate `dist/css/styles.css` from `app/less/_*.less`.

The file name can be customised:

    horde.demand('initConfig.less.cssFileName', 'my-app.css');  // generates `dist/css/my-app.css`

For more advanced customisation use the standard Horde override technique:

    horde.demand('initConfig.less.dev.files', {'dist/css/my-app.css' : 'path/to/less/root.less'});


## Task Aliases
You can customise the tasks that are executed for the following task aliases:

    horde.demand("registerTask.configuredTasks.default", ["clean", "less:dist"])

 - default: build
 - qa: jshint, test, sonar
 - build: newer:less:dev, newer:copy
 - dist: clean:dist, qa, less:dist, copy:dist, img

### closure:dist, closure
You can customise the tasks that are executed for all `closure` targets:

    horde.demand("registerTask.configuredTasks.closure", ["task1", "task2"])

...or for a specific target:

    horde.demand("registerTask.configuredTasks.closure.dev", ["task1", "task2"])

### serve:test, serve:proxy, serve
You can customise the tasks that are executed for all `serve` targets:

    horde.demand("registerTask.configuredTasks.serve", ["task1", "task2"])

...or for a specific target:

    horde.demand("registerTask.configuredTasks.serve.proxy", ["task1", "task2"])
