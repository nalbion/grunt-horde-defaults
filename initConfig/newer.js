// https://github.com/tschaub/grunt-newer/issues/29

var fs = require('fs');
var path = require('path');

function checkForNewerImports(lessFile, mTime, include) {
	fs.readFile(lessFile, "utf8", function(err, data) {
		var lessDir = path.dirname(lessFile),
			regex = /@import "(.+?)(\.less)?";/g,
			shouldInclude = false,
			match;

		while ((match = regex.exec(data)) !== null) {
			// All of my less files are in the same directory,
			// other paths may need to be traversed for different setups...
			var importFile = lessDir + '/' + match[1] + '.less';
			if (fs.existsSync(importFile)) {
				var stat = fs.statSync(importFile);
				if (stat.mtime > mTime) {
					shouldInclude = true;
					break;
				}
			}
		}
		include(shouldInclude);
	});
}

module.exports = function(grunt) {
	'use strict';

	return {
		/* For use with grunt-newer */
		options: {
			override: function(task, include) {
//console.info('task: ' + task);
//console.info('include: ' + include);
				if ('less' === task.task) {
					// call include with `true` if there are newer imports
					checkForNewerImports(task.path, task.time, include);
//				} else if( 'closureDepsWriter' === task.task ) {
//					console.info('newer.js -> DEBUGGING closureDepsWriter');
//					console.info(grunt.tasks);
//					console.info(task.options);
				} else {
					include(false);
				}
			}
		}
		/* For grunt-newer-explicit
		less: {
			src: "app/less/*.less",
			dest: "dist/css/style.css",
			options: { tasks: [ "copy:foo" ] }
		}*/
	};
};
