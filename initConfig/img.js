module.exports = function() {
	'use strict';

	return {
		dist: {
			src: 'app/images/**/*.png',
			dest: 'dist/images'
		}
	};
};