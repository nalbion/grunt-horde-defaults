module.exports = function(grunt) {
	'use strict';

	var fs = require('fs');

	var closureConfig = grunt.file.readJSON('grunt-closure.json');
	var closureHome = process.env['CC_HOME'] || '.';
	var rootWithPrefix = new Array();
	if( closureConfig.soySrcPath ) {
		rootWithPrefix.push('"' + closureConfig.soySrcPath + ' ../js"');	// ' ../../../soy"'
		rootWithPrefix.push('"' + closureHome + '/closure-library/template ../../../template"');
	}
	if( closureConfig.componentPath ) {
		rootWithPrefix.push('"' + closureConfig.componentPath + ' ../../components"');	// ../../../closure-bootstrap
		var soyPath = closureConfig.componentPath + '/soy'; //path.resolve(closureConfig.componentPath, 'soy');
		if( fs.existsSync(soyPath) ) {
			rootWithPrefix.push('"' + soyPath + ' ../../components/soy"');
		}
	}

	return {
		options: {
			closureLibraryPath: closureConfig.closureLibrary
		},
		app: {
			options: {
				root_with_prefix: rootWithPrefix.concat('"' + closureConfig.jsSrcPath + ' ../js"') // ' ../../../js-closure"',
			},
			src: closureConfig.jsSrcPath + '/**/*.js',	// helps "newer" figure out which files to check
// "newer:closureDepsWriter" requires at least one valid file in .src, but whatever goes in here
// breaks the deps file because it's not relative to goog/base.js
//			src: [
//				closureConfig.jsSrcPath + closureConfig.bootstrapFile
//			],
			dest: closureConfig.jsSrcPath + 'deps.js'
		},
		bddTest: {
			options: {
				root_with_prefix: [
					'"test ../../../../test"',
					'"' + closureConfig.jsSrcPath + ' ../../js"'
//					'"' +closureConfig.componentPath + ' ../../components"'
				]
			},
			src: [closureConfig.jsSrcPath + '/**/*.js', 'test/**/.js'],
			dest: 'test/bdd/deps-test-bdd.js'
		},
		unitTest: {
			options: {
				root_with_prefix: [
					'"test ../../../../test"',
					'"' + closureConfig.jsSrcPath + ' ../../js"'
//					'"' + closureConfig.componentPath + ' ../../components"'
				]
			},
			src: [closureConfig.jsSrcPath + '/**/*.js', 'test/**/.js'],
			dest: 'test/unit/deps-test-tdd.js'
		}
	};
};