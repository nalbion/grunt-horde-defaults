// Copies remaining files to places other tasks can use
module.exports = function(grunt) {
	'use strict';

	var closureConfig = grunt.file.readJSON('grunt-closure.json');
	var devSrc = [
		'mockup/*.html',
		'icons/**',
		'images/**',
		'**/deps.js'
	];
	if( closureConfig && closureConfig.bootstrapFile ) {
		devSrc.concat( '../' + closureConfig.jsSrcPath + closureConfig.bootstrapFile );
	}

	return {
		dev: {
			files: [ {
				expand: true,
				dot: true,
				cwd: 'app',
				dest: 'dist',
				src: devSrc
			} ]
		},
		dist: {
			files: [{
				expand: true,
				dot: true,
				cwd: 'app',
				dest: 'dist',
				src: [
					'*.{ico,png,txt}'
//					'.htaccess',
//					'*.html',
//					'views/{,*/}*.html',
//					'images/{,*/}*.*'
//					'fonts/*'
				]
//			}, {
//				expand: true,
//				cwd: '.temp/images',
//				dest: 'dist/images',
//				src: ['generated/*']
			}
			]
		}
	};
};