// Start a connect web server on port 8000
// see: https://github.com/gruntjs/grunt-contrib-connect
// To add extra paths to the middleware path list call:
// <code>horde.demand('initConfig.connect.extraMiddlewarePaths', grunt.file.readJSON('grunt-closure.json').closureLibrary)</code>
module.exports = function(grunt) {
	'use strict';

	/** @type {Array.<Function>|Array.<string>|Function|string} */
	var extraMiddleware = this.learn('initConfig.connect.extraMiddleware');
	var hostname = this.learn('initConfig.connect.hostname') || 'localhost';

	/**
	 * @return {middleware} - function(req, res, next)
	 * @see https://github.com/gruntjs/grunt-contrib-connect#middleware
	 */
	var mountFolder = function (connect, dir) {
		return connect.static(require('path').resolve(dir));
	};

	/**
	 * @param connect
	 * @param {Array} middlewareArray
	 * @param {Function|string} mw
	 */
	var pushMiddleware = function(connect, middlewareArray, mw) {
		if( mw.constructor == String ) {
			mw = mountFolder(connect, mw);
		}
		middlewareArray.push( mw );
	};

	/**
	 * @param connect
	 * @param {Array} middlewareArray
	 */
	var addExtraMiddleware = function(connect, middlewareArray) {
		var extraMw = [];
		if( undefined !== extraMiddleware ) {
			if( extraMiddleware.forEach ) {
				extraMiddleware.forEach( function(mw){
					pushMiddleware(connect, extraMw, mw);
				});
			} else {
				pushMiddleware(connect, extraMw, extraMiddleware);
			}
		}
		return extraMw.concat(middlewareArray);
	};

	return {
		options: {
			// debug: false,
			// port: 9000,
			base: 'dist',
			// livereload: false, (leave as default - our JSPs are behind DevelopmentGruntConnectFilter)
			// keepalive: false,  No need for keepalive anymore as watch will keep Grunt running
			// change this to '0.0.0.0' to access the server from outside
			hostname: hostname //'0.0.0.0' //'192.168.0.0' //'localhost'
		},
		// if you only want the JS, CSS and static html
		// run "grunt 'connect:front-end'" and browse to http://localhost:8000
		frontend: {
			options: {
//				base: 'app',
				port: 8000,
				// Livereload needs connect to insert a javascript snippet in the pages it serves.
				// (looking at the source, it seems that this only works for "/" and "*.html"
				// ...so nested pages may require injecting the script manually:
				// "<script>document.write('<script src=\"http://' + (location.host || 'localhost').split(':')[0] + ':"
				// 	+ port + "/livereload.js?snipver=1\"><\\/script>"
				// This requires using a custom connect middleware
				middleware: function(connect, options) {
					if (options.base instanceof Array) {
						options.base = options.base[0];
					}
					return addExtraMiddleware(connect,  [
						// Load the middleware provided by the livereload plugin
						// that will take care of inserting the snippet
						require('grunt-contrib-livereload/lib/utils').livereloadSnippet,

						// Serve the project folder
						connect.static(options.base)]);
				}
			}
		},
		// run "grunt 'connect:frontend' 'connect:proxy'" and browse to http://localhost:8001
		// (or enable hit the back-end directly on 8080 and let DevelopmentGruntConnectFilter
		// redirect /js, /css etc to our "grunt connect:frontend"
		proxy: {
			options: {
				port: 8001,
				middleware: function(connect) {
					return [
						require('grunt-connect-proxy/lib/utils').proxyRequest	// enable the 'proxies' section below
					];
				}
			},
			proxies: [
//				{ context: '/', host: 'www.facebook.com', port: 9000 }, ...I don't think this will work - wrong way around...(TODO: might help with testing if we can get this working...)
				// send all /js requests to the 'front-end' connect server
				{ context: '/js', host: 'localhost', port: 8000 },
////				{ context: '/api', host: 'localhost', port: 9080 },
				// redirect anything else to the java back-end server
				{ context: '/', host: 'localhost', port: 8080 }
			]
		},
		test: {
			options: {
				port: 4242,
				middleware: function(connect) {
					return addExtraMiddleware(connect, [connect.static(options.base)]);
				}
			}
		}
	};
};