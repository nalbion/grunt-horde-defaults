module.exports = function(grunt) {
	'use strict';

	var compiler = require('superstartup-closure-compiler');
	// process.env['CC_HOME']
	var closureConfig = grunt.file.readJSON('grunt-closure.json');
	var closureHome = process.env['CC_HOME'] || '.';
	var destPath = closureConfig.destPath;
	var destName = this.learn('initConfig.closureBuilder.destName')
		|| (grunt.file.readJSON('package.json').name + '.js');

	grunt.file.mkdir(destPath);

	var src = [
		closureConfig.jsSrcPath,
		closureConfig.closureLibrary + '/closure',
		closureConfig.closureLibrary + '/third_party'
	];
	if( closureConfig.soySrcPath ) {
		src.push(closureHome + '/closure-library/template');
		src.push(closureConfig.soySrcPath);

	}
	if( closureConfig.componentPath && closureConfig.components ) {
		for( var i = 0; i < closureConfig.components.length; i++ ) {
			src.push(closureConfig.componentPath + closureConfig.components[i]);
		}
	}

	return {
		options: {
			closureLibraryPath: closureConfig.closureLibrary,
			inputs: [closureConfig.jsSrcPath + '/' + closureConfig.bootstrapFile],
			compile: true,
			compilerFile: compiler.getPath(),
			compilerOpts: {
//				'js bower_components\\closure-library\\closure\\goog\\deps.js': null,
				js: closureConfig.closureLibrary + 'closure/goog/deps.js',
				compilation_level: 'ADVANCED_OPTIMIZATIONS',
//				externs: [closureConfig.externsPath + '*.js'],
//				define: [
//					'\'goog.DEBUG=false\''
//				],
				warning_level: 'verbose',
				jscomp_off: ['checkTypes', 'fileoverviewTags']
//				summary_detail_level: 3,
//				only_closure_dependencies: null,
//				closure_entry_point: closureConfig.entryPoint,
//				create_source_map: closureConfig.sourceMap,
//				source_map_format: 'V3',
//				output_wrapper: closureConfig.outputWrapper
			}
		},
		dist: {
			src: src,
			//dest: 'dist/js/<%= destName %>.js'
			dest: destPath + destName
		},
//		xtb: {
//			src: src,
//			inputs: [closureConfig.jsSrcPath + '/' + closureConfig.bootstrapFile],
//			compile: true,
//			compilerFile: closureHome + '/XtbGenerator.jar',
//			// --jvm_flags="${JVM_ARCH}" \
//			compilerOpts: {
//				translations_file=${JS_APP_MESSAGES},	'messages.xtb',		// load locale-specific messages from here (https://github.com/kuzmisin/xtbgenerator will append to this file)
//				xtb_output_file: 'messages.xtb', //${JS_APP_MESSAGES},
//				lang: 'cs', //${LANG}
//			}
//		}
		dev: {
			options: {
				compile: true,
//				compilerFile: compiler.getPath()
				compilerOpts: {
					compilation_level: 'WHITESPACE_ONLY',
									// 'SIMPLE_OPTIMIZATIONS '
									//	'ADVANCED_OPTIMIZATIONS',
					create_source_map: closureConfig.sourceMap,
					source_map_format: 'V3'
//					,message_bundle:
//					output_wrapper: closureConfig.outputWrapper,
//					should_generate_goog_msg_defs: null
//					,define: [
//						"'goog.LOCALE=fr'"
//					]
				}

			},
			src: src,
			dest: destPath + destName
		}
	};
};