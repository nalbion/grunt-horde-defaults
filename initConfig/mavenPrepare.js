// https://github.com/allegro/grunt-maven-npm
// copy additional raw sources from Maven webapp sources to target-grunt
module.exports = function() {
	'use strict';

	return {
		options: {
			resources: ['images/**', 'less/**', 'WEB-INF/js-closure/**', 'WEB-INF/soy/**']
		},
		dev: {
		},
		dist: {
			resources: ['**', '!dev/**']
		}
	};
};