// https://www.npmjs.org/package/grunt-sync
// similar to grunt-contrib-copy but tries to copy only files which have actually changed.
module.exports = function() {
	'use strict';

	return {
		dev: {
			verbose: true,
			files: [ {
				cwd: 'app',
				src: [
					'mockup/*.html',
					'**/deps.js'
				],
				dest: 'dist'
			} ]
		}
	};
};