module.exports = function() {
	'use strict';

	var defOptions = {
		node: true,
		esnext: true
	};

	return {
		options: {
			jshintrc: '.jshintrc'
//			reporter: require('jshint-stylish')
		},
		src: {
			options: defOptions,
			files: {
				src: ['app/js/**/*.js', 'app/js-closure/**/*.js']
			}
		},
		grunt: {
			files: {
				src: ['Gruntfile.js']
			}
		},
		tests: {
			options: this.assimilate(defOptions, {expr: true}),
			files: {
				src: ['test/**/*.js']
			}
		},
		json: {
			files: {
				src: ['*.json']
			}
		}
	};
};