// Defines top-level keys passed to grunt.initConfig
module.exports = function(grunt) {
  'use strict';

  return {
	  package: grunt.file.readJSON('package.json'),
	  mavenConfig: grunt.file.readJSON('grunt-maven.json'),
	  closureConfig: grunt.file.readJSON('grunt-closure.json')
  };
};
