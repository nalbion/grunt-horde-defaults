// Automatically open the browser at a given path
module.exports = function() {
	'use strict';

	return {
		dev: {
			app: 'chrome',	// if not specified, default browser will be used
			path: 'http://localhost:' + (this.learn('initConfig.connect.options.port') || 8000) + '/mockup/index.html'
		},
		test: {
			app: 'chrome',	// if not specified, default browser will be used
			path: 'http://localhost:' + (this.learn('initConfig.connect.test.options.port') || 8000) + '/test/index.html'
		}
	};
};