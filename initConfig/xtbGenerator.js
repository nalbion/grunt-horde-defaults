// Generate XTB base file from goog.getMsg('') calls
module.exports = function(grunt) {
	'use strict';

	var fs = require('fs');

	var closureConfig = grunt.file.readJSON('grunt-closure.json');
	var closureHome = process.env['CC_HOME'] || '.';
	var src = [
		closureConfig.jsSrcPath,
		closureConfig.closureLibrary + '/closure',
		closureConfig.closureLibrary + '/third_party'
	];
	if( closureConfig.soySrcPath ) {
		src.push(closureHome + '/closure-library/template');
		src.push(closureConfig.soySrcPath);
	}
	if( closureConfig.componentPath && closureConfig.components ) {
		for( var i = 0; i < closureConfig.components.length; i++ ) {
			src.push(closureConfig.componentPath + closureConfig.components[i]);
//			var soyPath = closureConfig.componentPath + closureConfig.components[i] + '/soy';
//			if( fs.existsSync(soyPath) ) {
//				src.push(soyPath);
//			}
		}
	}

	return {
		generate: {
			options: {
				builder: 'node_modules/grunt-xtb-generator/bin/build/closurebuilder.py',
				root: src,
				input: closureConfig.jsSrcPath + '/' + closureConfig.bootstrapFile,
				compiler_jar: 'node_modules/grunt-xtb-generator/bin/XtbGenerator.jar',
				compiler_flags: {
					translations_file: 'app/messages.xtb',
					xtb_output_file: 'app/messages.xtb',
					lang: 'cs'
				}
			}
		}
	};
};