module.exports = function() {
	'use strict';

	return {
		// https://github.com/thanpolas/grunt-closure-tools/pull/48#issuecomment-46270883
		closure_source_map_paths: {
			src: 'dist/js/*.js.map',
			overwrite: true,
			replacements: [
				// Replace paths like 'app\\shared\\bla.js' with '..\\shared\\bla.js'
				// 'to' should be relative path to root from where the source map is located
				{ from: '"bower_components\\\\', to: '"..\\\\' },
				{ from: '"bower_components\\\\', to: '"..\\\\' },
				{ from: /"app\\\\(js[-\w]*\\\\)?/g, to: '"..\\\\' }
			]
		}
	};
};