// Copy Bower installed resources into our workspace (if we need fonts or bootstrap.js)
// Bootstrap .less files are not copied - our .less files need to find them in ../bower_components/bootstrap/less
// see: https://github.com/curist/grunt-bower
// alternatives: https://github.com/mavdi/grunt-bower-organiser, https://github.com/yatskevich/grunt-bower-task
module.exports = function() {
	'use strict';

	return {
		dev: {
			dest: 'dist/3rdparty',
			js_dest: 'dist/js/3rdparty',
			css_dest: 'dist/css/3rdparty',
			options: {
				packageSpecific: {
					bootstrap: {
						dest: 'dist/fonts',
						css_dest: '.temp',
						less_dest: 'app/less/common/bootstrap'
					}
				}
			}
		}
	};
};