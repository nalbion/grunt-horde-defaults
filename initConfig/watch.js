module.exports = function() {
	'use strict';

	return {
		less: {
			files: ['app/less/**/*.less', '!app/less/common/**'],
			tasks: ['less:dev'],
			options: { livereload: true }
		},
		closure: {
			files: ['app/js-closure/**/*.js'],
			tasks: ['closure:dev'],
			options: { livereload: true }
		},
		html: {
			files: ['app/mockup/*.html'],
			tasks: ['sync'],
			options: { livereload: true }
		},
//		jsTest: {
//			files: ['test/**/*.js'],
//			tasks: ['newer:jshint:test', 'karma']
//		},
//		gruntfile: {
//			files: ['Gruntfile.js']
//		},
		livereload: {
			options: {
				livereload: '<%= connect.options.livereload %>'
			},
			files: [
				'app/{,*/}*.html',
//				'.temp/styles/{,*/}*.css',
				'app/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
			]
		}
	};
};