module.exports = function() {
	'use strict';

	var cssFileName = this.learn('initConfig.less.cssFileName') || 'styles.css';
	var files = {};
	files['dist/css/' + cssFileName] = "app/less/_*.less";

	return {
		dev: {
			options: {
//				paths: ["assets/css"],
				sourceMap: true,
				report: true
			},
			files: files
//			files: {
//				"dist/css/styles.css": "app/less/_*.less"
//			}
		},
		dist: {
			options: {
//				paths: ["assets/css"],
				cleancss: true,
				report: true
			},
			files: files
//			files: {
//				"dist/css/styles.css": "app/less/_*.less"
//			}
		}
	};
};