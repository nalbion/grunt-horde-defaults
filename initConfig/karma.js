module.exports = function() {
	'use strict';

	return {
		unit: {
			configFile: 'karma.conf.js',
			singleRun: true
		}
	};
};