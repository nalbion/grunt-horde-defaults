module.exports = function() {
	'use strict';

	return {
		test: {
			tasks: [
//					'clean:frontend',
					'connect:test',
					'open:test',
					'watch:test'
			]
		},
		proxy: {
			tasks: [
//				'clean:server',
				'connect:frontend',
				'configureProxies:proxy', 'connect:proxy',
				'open:dev',
				'watch:livereload'
			]
		},
		dev: {
			tasks: [
				'clean:server',
				'connect:frontend',
				'open:dev',
				'watch'
			]
		}
	};
};