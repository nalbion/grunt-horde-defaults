module.exports = function() {
	'use strict';

	return {
		dist: {
			files: [{
				expand: true,
				cwd: 'app/images-raw',
				src: '{,*/}*.{png,jpg,jpeg}',
				dest: 'dist/images'
			}]
		}
	};
};