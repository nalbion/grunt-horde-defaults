// clean, uglify and concat aid in building
module.exports = function() {
	'use strict';

	return {
		dist: ['.temp', 'dist'],
		server: '.temp'
	};
};