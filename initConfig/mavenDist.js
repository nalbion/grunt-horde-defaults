module.exports = function() {
	'use strict';

	return {
		options: {
			warName: 'war',
			deliverables: ['**', '!non-deliverable.js'],
			gruntDistDir: 'dist'
		},
		dev: {
			warName: 'war-dev'
		}
	};
};