// https://www.npmjs.org/package/grunt-closure-soy
module.exports = function(grunt) {
	'use strict';

	// http://closure-templates.googlecode.com/files/closure-templates-for-javascript-latest.zip
	var closureConfig = grunt.file.readJSON('grunt-closure.json');
	var closureHome = process.env['CC_HOME'] || '.';

	var fs = require('fs');

	var soyToJsJarPath = closureHome + '/closure-library/template/SoyToJsSrcCompiler.jar';
	if( !fs.existsSync(soyToJsJarPath) ) {
		console.warn('Warning: ' + soyToJsJarPath + ' does not exist, you may need to define CC_HOME');
	}

	return {
		all: {
			src: [closureConfig.soySrcPath + '/**/*.soy',
				closureConfig.componentPath + 'jobapp-common-ui/soy/*.soy'],
			soyToJsJarPath: closureHome + '/closure-library/template/SoyToJsSrcCompiler.jar',
			/** @optional  - defaults to '{INPUT_DIRECTORY}/{INPUT_FILE_NAME}.js' */
			outputPathFormat: '{INPUT_DIRECTORY}/{INPUT_FILE_NAME}.js',
			/** any other parameter included on the options will be added to call */
			options: {
				shouldGenerateJsdoc: true,
				shouldProvideRequireSoyNamespaces: true
			}
		}
	};
};